package fr.uavignon.ceri.tp3;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.List;

import fr.uavignon.ceri.tp3.data.WeatherRepository;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    public TextView textViewResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewResult = findViewById(R.id.text_view_result);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://demo-lia.univ-avignon.fr/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<List<Contenu>> call = jsonPlaceHolderApi.getContenu();

        call.enqueue(new Callback<List<Contenu>>() {
            @Override
            public void onResponse(Call<List<Contenu>> call, Response<List<Contenu>> response) {

                if (!response.isSuccessful()) {
                    textViewResult.setText("Code" + response.code());
                    return;
                }

                List<Contenu> contenus = response.body();
                for(Contenu contenu : contenus) {
                    String content ="";
                    content += "name" + contenu.getName() + "\n";


                    content += "description" + contenu.getDescription() + "\n";

                    textViewResult.append(content);
                }
            }



            @Override
            public void onFailure(Call<List<Contenu>> call, Throwable t) {
                    textViewResult.setText(t.getMessage());
            }
        });


    }

}