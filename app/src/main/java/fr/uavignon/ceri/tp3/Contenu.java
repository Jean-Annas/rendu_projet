package fr.uavignon.ceri.tp3;

public class Contenu {

    public String name;
    public int [] timeFrame;
    public String categories;
    public String description;
    public String technicalDetails;
    public String picture;

    public String getName() {
        return name;
    }

    public int[] getTimeFrame() {
        return timeFrame;
    }

    public String getCategories() {
        return categories;
    }

    public String getDescription() {
        return description;
    }

    public String getTechnicalDetails() {
        return technicalDetails;
    }

    public String getPicture() {
        return picture;
    }
}
