package fr.uavignon.ceri.tp3;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonPlaceHolderApi {

@GET("cerimuseum/collection")
    Call<List<Contenu>> getContenu();


}


